#!/bin/bash

mkdir -p bin

version=$(cd ../devel && ./build.sh && ./bin/version)
if [ -z "${version}" ]; then
  version="0.0"
fi

cp -f strings.xml.dist res/values/strings.xml
sed -i "s/VERSION/${version}/g" res/values/strings.xml
