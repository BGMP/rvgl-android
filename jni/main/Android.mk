LOCAL_PATH := $(call my-dir)
SDL_PATH := ../SDL2
CORE_PATH := ../../../core
DEVEL_PATH := ../../../devel
PLATFORM_PATH := ../../../distrib/platform

include $(CLEAR_VARS)

# GLAD
LOCAL_MODULE := glad
LOCAL_SRC_FILES := $(DEVEL_PATH)/android/lib/$(TARGET_ARCH_ABI)/libglad.a
include $(PREBUILT_STATIC_LIBRARY)
include $(CLEAR_VARS)

# ALURE
LOCAL_MODULE := alure
LOCAL_SRC_FILES := $(DEVEL_PATH)/android/lib/$(TARGET_ARCH_ABI)/libalure.a
include $(PREBUILT_STATIC_LIBRARY)
include $(CLEAR_VARS)

# Ogg
LOCAL_MODULE := ogg
LOCAL_SRC_FILES := $(DEVEL_PATH)/android/lib/$(TARGET_ARCH_ABI)/libogg.a
include $(PREBUILT_STATIC_LIBRARY)
include $(CLEAR_VARS)

# Vorbis
ifeq ($(TARGET_ARCH),$(filter $(TARGET_ARCH),arm arm64))
LOCAL_MODULE := vorbisidec
LOCAL_SRC_FILES := $(DEVEL_PATH)/android/lib/$(TARGET_ARCH_ABI)/libvorbisidec.a
include $(PREBUILT_STATIC_LIBRARY)
include $(CLEAR_VARS)

VORBIS_LIBS := vorbisidec
else
LOCAL_MODULE := vorbis
LOCAL_SRC_FILES := $(DEVEL_PATH)/android/lib/$(TARGET_ARCH_ABI)/libvorbis.a
include $(PREBUILT_STATIC_LIBRARY)
include $(CLEAR_VARS)

LOCAL_MODULE := vorbisfile
LOCAL_SRC_FILES := $(DEVEL_PATH)/android/lib/$(TARGET_ARCH_ABI)/libvorbisfile.a
include $(PREBUILT_STATIC_LIBRARY)
include $(CLEAR_VARS)

VORBIS_LIBS := vorbisfile vorbis
endif

# FLAC
LOCAL_MODULE := FLAC
LOCAL_SRC_FILES := $(DEVEL_PATH)/android/lib/$(TARGET_ARCH_ABI)/libFLAC.a
include $(PREBUILT_STATIC_LIBRARY)
include $(CLEAR_VARS)

# ENet
LOCAL_MODULE := enet
LOCAL_SRC_FILES := $(DEVEL_PATH)/android/lib/$(TARGET_ARCH_ABI)/libenet.a
include $(PREBUILT_STATIC_LIBRARY)
include $(CLEAR_VARS)

# OpenAL
LOCAL_MODULE := openal
LOCAL_SRC_FILES := $(PLATFORM_PATH)/android/$(TARGET_ARCH_ABI)/libopenal.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)

# MPG123
LOCAL_MODULE := mpg123
LOCAL_SRC_FILES := $(PLATFORM_PATH)/android/$(TARGET_ARCH_ABI)/libmpg123.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)

# SndFile
LOCAL_MODULE := sndfile
LOCAL_SRC_FILES := $(PLATFORM_PATH)/android/$(TARGET_ARCH_ABI)/libsndfile.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)

# Unistring
LOCAL_MODULE := unistring
LOCAL_SRC_FILES := $(PLATFORM_PATH)/android/$(TARGET_ARCH_ABI)/libunistring.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)

# RVGL
LOCAL_MODULE := main

ifdef PREBUILT
LOCAL_SRC_FILES := $(PLATFORM_PATH)/android/$(TARGET_ARCH_ABI)/libmain.so
include $(PREBUILT_SHARED_LIBRARY)
else
LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(DEVEL_PATH)/android/include \
                    $(LOCAL_PATH)/$(DEVEL_PATH)/include \
                    $(LOCAL_PATH)/$(CORE_PATH)/include

LOCAL_SRC_FILES := $(SDL_PATH)/src/main/android/SDL_android_main.c \
                   $(subst $(LOCAL_PATH)/,, \
                   $(wildcard $(LOCAL_PATH)/$(CORE_PATH)/src/*.cpp))

LOCAL_SHARED_LIBRARIES := SDL2 SDL2_image openal mpg123 sndfile unistring
LOCAL_STATIC_LIBRARIES := glad enet alure $(VORBIS_LIBS) FLAC ogg

LOCAL_ARM_MODE := arm

#LOCAL_CPP_FEATURES += rtti
#LOCAL_CPPFLAGS := -std=gnu++0x

LOCAL_LDLIBS := -llog

include $(BUILD_SHARED_LIBRARY)
endif

include $(CLEAR_VARS)
